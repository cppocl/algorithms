/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../fibonacci/fibonacci.hpp"

TEST(fibonacci)
{
    std::size_t n = 50;
    std::size_t loops = 10000;
    std::size_t f1 = 0;
    std::size_t f2 = 0;
    std::size_t f3 = 0;

    // Time the map solution.
    clock_t start = clock();
    std::map<std::size_t, std::size_t> map_cache;
    for (std::size_t l = 0; l < loops; ++l)
    {
        map_cache.clear();
        f1 = ocl::fibonacci(n, map_cache);
    }
    clock_t stop = clock();
    clock_t elapsed = stop - start;
    std::cout << "fibonacci elapsed (map): " << elapsed << ". value: " << f1 << std::endl;

    // Time the vector solution.
    start = clock();
    std::vector<std::size_t> vec_cache;
    for (std::size_t l = 0; l < loops; ++l)
    {
        vec_cache.clear();
        f2 = ocl::fibonacci(n, vec_cache);
    }
    stop = clock();
    elapsed = stop - start;
    std::cout << "fibonacci elapsed (vector): " << elapsed << ". value: " << f2 << std::endl;

    // Time the iterative solution.
    start = clock();
    for (std::size_t l = 0; l < loops; ++l)
        f3 = ocl::fibonacci(n);
    stop = clock();
    elapsed = stop - start;
    std::cout << "fibonacci elapsed: " << elapsed << ". value: " << f3 << std::endl;
}
