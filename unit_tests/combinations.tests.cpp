/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../combinations/combinations.hpp"
#include "../pow/pow.hpp"
#include "Print.hpp"
#include <algorithm>
#include <time.h>
#include <vector>

TEST_MEMBER_FUNCTION(Combinations, GetAll, NA)
{
    typedef ocl::Combinations<std::size_t> combinations_type;
    typedef typename combinations_type::type type;
    typedef std::vector<std::size_t> container_type;
    typedef std::vector<std::vector<std::size_t>> combinations_container_type;

    {
        container_type values{ 1, 2, 3 };
        combinations_container_type combs{};

        // Test all unique combinations (excluding an empty result)
        combinations_type::GetAll(combs, values);
        std::size_t expected = combinations_type::Count(values.size());
        CHECK_TRUE(combs.size() == expected);
    }

    {
        container_type values{ 1, 2, 3, 4 };
        combinations_container_type combs{};

        // Test all unique combinations (excluding an empty result)
        combinations_type::GetAll(combs, values);
        std::size_t expected = combinations_type::Count(values.size());
        CHECK_TRUE(combs.size() == expected);
    }

    {
        container_type values{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        combinations_container_type combs{};

        // Test all unique combinations (excluding an empty result)
        combinations_type::GetAll(combs, values);
        std::size_t expected = combinations_type::Count(values.size());
        CHECK_TRUE(combs.size() == expected);
    }
}

TEST_MEMBER_FUNCTION(Combinations, Get, NA)
{
    typedef ocl::Combinations<std::size_t> combinations_type;
    typedef typename combinations_type::type type;
    typedef std::vector<std::size_t> container_type;
    typedef std::vector<std::vector<std::size_t>> combinations_container_type;

    {
        container_type values{ 1, 2, 3 };
        combinations_container_type combs{};

        // Test all unique combinations containing 2 values.
        combinations_type::Get(combs, values, 2U);
        std::size_t expected = combinations_type::Count(values.size(), 2U);
        CHECK_TRUE(combs.size() == expected);
    }

    {
        container_type values{ 1, 2, 3, 4 };
        combinations_container_type combs{};

        // Test all unique combinations containing 2 values.
        // for (1, 2, 3, 4) combinations of length 2 = (1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)
        combinations_type::Get(combs, values, 2U);
        std::size_t expected = combinations_type::Count(values.size(), 2U);
        CHECK_TRUE(combs.size() == expected);
    }

    {
        container_type values{ 1, 2, 3, 4 };
        combinations_container_type combs{};

        combinations_type::Get(combs, values, 3U);
        std::size_t expected = combinations_type::Count(values.size(), 3U);
        CHECK_TRUE(combs.size() == expected);
    }

    {
        container_type values{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16 };
        combinations_container_type combs{};

        // Test all unique combinations containing 2 values.
        combinations_type::Get(combs, values, 2U);
        std::size_t expected = combinations_type::Count(values.size(), 2);
        CHECK_TRUE(combs.size() == expected);
    }
}

#if __has_include("combinations.performance.tests.cpp")
#include "combinations.performance.tests.cpp"
#endif
