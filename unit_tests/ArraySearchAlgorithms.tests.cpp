/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../array_search_algorithms/ArraySearchAlgorithms.hpp"

using ocl::ArraySearchAlgorithm;

TEST_MEMBER_FUNCTION(ArraySearchAlgorithm, BinarySearch, int_const_ptr_size_t_int_size_t_ref)
{
    typedef size_t size_type;

    static int const values[10] = {2, 3, 5, 6, 7, 9, 10, 13, 14, 16};
    size_type const size = sizeof(values) / sizeof(values[0]);

    static int const invalid_search_values[10] = {-1, 0, 1, 4, 8, 11, 12, 15, 17, 18};

    size_type nearest_pos = size;
    bool found;

    TEST_OVERRIDE_ARGS("int const*, size_t, int, size_t&");

    // Test failure conditions.

    // Verify 0 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 0U, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify null array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(NULL, 1U, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify 1 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 1U, 1, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify 1 length array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 1U, 3, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 1U);

    // Verify array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, size, 17, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, size);

    // Search in array from length 1 to 10, for all invalid values.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int invalid_value = invalid_search_values[pos];
            found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, array_len, invalid_value, nearest_pos);
            CHECK_FALSE(found);
        }
    }

    // Test pass conditions.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int valid_value = values[pos];
            found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, array_len, valid_value, nearest_pos);
            CHECK_TRUE(found);
        }
    }
}

TEST_MEMBER_FUNCTION(ArraySearchAlgorithm, BinarySearch, int_const_ptr_unsigned_long_int_unsigned_long_ref)
{
    typedef unsigned long size_type;

    static int const values[10] = {2, 3, 5, 6, 7, 9, 10, 13, 14, 16};
    size_type const size = sizeof(values) / sizeof(values[0]);

    static int const invalid_search_values[10] = {-1, 0, 1, 4, 8, 11, 12, 15, 17, 18};

    size_type nearest_pos = size;
    bool found;

    TEST_OVERRIDE_ARGS("int const*, unsigned long, int, unsigned long&");

    // Test failure conditions.

    // Verify 0 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 0U, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify null array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(NULL, 1U, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify 1 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 1U, 1, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify 1 length array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 1U, 3, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 1U);

    // Verify array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, size, 17, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, size);

    // Search in array from length 1 to 10, for all invalid values.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int invalid_value = invalid_search_values[pos];
            found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, array_len, invalid_value, nearest_pos);
            CHECK_FALSE(found);
        }
    }

    // Test pass conditions.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int valid_value = values[pos];
            found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, array_len, valid_value, nearest_pos);
            CHECK_TRUE(found);
        }
    }
}

TEST_MEMBER_FUNCTION(ArraySearchAlgorithm, BinarySearch, int_const_ptr_signed_long_int_signed_long_ref)
{
    typedef signed long size_type;

    static int const values[10] = {2, 3, 5, 6, 7, 9, 10, 13, 14, 16};
    size_type const size = sizeof(values) / sizeof(values[0]);

    static int const invalid_search_values[10] = {-1, 0, 1, 4, 8, 11, 12, 15, 17, 18};

    size_type nearest_pos = size;
    bool found;

    TEST_OVERRIDE_ARGS("int const*, signed long, int, signed long&");

    // Test failure conditions.

    // Verify 0 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 0, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0);

    // Verify null array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(NULL, 1, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0);

    // Verify 1 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 1, 1, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0);

    // Verify 1 length array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, 1, 3, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 1);

    // Verify array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, size, 17, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, size);

    // Search in array from length 1 to 10, for all invalid values.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int invalid_value = invalid_search_values[pos];
            found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, array_len, invalid_value, nearest_pos);
            CHECK_FALSE(found);
        }
    }

    // Test pass conditions.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int valid_value = values[pos];
            found = ArraySearchAlgorithm<int, size_type>::BinarySearch(values, array_len, valid_value, nearest_pos);
            CHECK_TRUE(found);
        }
    }
}


// test sequential search

TEST_MEMBER_FUNCTION(ArraySearchAlgorithm, SequentialSearch, int_const_ptr_unsigned_long_int_unsigned_long_ref)
{
    typedef unsigned long size_type;

    static int const values[10] = {2, 3, 5, 6, 7, 9, 10, 13, 14, 16};
    size_type const size = sizeof(values) / sizeof(values[0]);

    static int const invalid_search_values[10] = {-1, 0, 1, 4, 8, 11, 12, 15, 17, 18};

    size_type nearest_pos = size;
    bool found;

    TEST_OVERRIDE_ARGS("int const*, unsigned long, int, unsigned long&");

    // Test failure conditions.

    // Verify 0 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, 0U, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify null array search fails.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(NULL, 1U, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify 1 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, 1U, 1, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0U);

    // Verify 1 length array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, 1U, 3, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 1U);

    // Verify that searching for 12 will find 13 as the nearest value.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, size, 12, nearest_pos);
    CHECK_FALSE(found);
    CHECK_TRUE((nearest_pos < size) && (values[nearest_pos] == 13));
    CHECK_EQUAL(nearest_pos, 7U);

    // Verify array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, size, 17, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, size);

    // Search in array from length 1 to 10, for all invalid values.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int invalid_value = invalid_search_values[pos];
            found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, array_len, invalid_value, nearest_pos);
            CHECK_FALSE(found);
        }
    }

    // Test pass conditions.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int valid_value = values[pos];
            found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, array_len, valid_value, nearest_pos);
            CHECK_TRUE(found);
        }
    }
}

TEST_MEMBER_FUNCTION(ArraySearchAlgorithm, SequentialSearch, int_const_ptr_signed_long_int_signed_long_ref)
{
    typedef signed long size_type;

    static int const values[10] = {2, 3, 5, 6, 7, 9, 10, 13, 14, 16};
    size_type const size = sizeof(values) / sizeof(values[0]);

    static int const invalid_search_values[10] = {-1, 0, 1, 4, 8, 11, 12, 15, 17, 18};

    size_type nearest_pos = size;
    bool found;

    TEST_OVERRIDE_ARGS("int const*, signed long, int, signed long&");

    // Test failure conditions.

    // Verify 0 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, 0, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0);

    // Verify null array search fails.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(NULL, 1, 2, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0);

    // Verify 1 length array search fails.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, 1, 1, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 0);

    // Verify 1 length array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, 1, 3, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, 1);

    // Verify array search fails with match past end of array.
    found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, size, 17, nearest_pos);
    CHECK_FALSE(found);
    CHECK_EQUAL(nearest_pos, size);

    // Search in array from length 1 to 10, for all invalid values.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int invalid_value = invalid_search_values[pos];
            found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, array_len, invalid_value, nearest_pos);
            CHECK_FALSE(found);
        }
    }

    // Test pass conditions.
    for (size_type array_len = 1; array_len <= size; ++array_len)
    {
        for (size_type pos = 0; pos < array_len; ++pos)
        {
            int valid_value = values[pos];
            found = ArraySearchAlgorithm<int, size_type>::SequentialSearch(values, array_len, valid_value, nearest_pos);
            CHECK_TRUE(found);
        }
    }
}
