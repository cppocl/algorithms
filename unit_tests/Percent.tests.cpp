/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../percent/Percent.hpp"

using ocl::Percent;

TEST_MEMBER_FUNCTION(Percent, CalculatePercent, type_type)
{
    CHECK_EQUAL(Percent<int>::CalculatePercent(10, 20), 50);
    CHECK_EQUAL(Percent<int>::CalculatePercent(40, 160), 25);
    CHECK_EQUAL(Percent<int>::CalculatePercent(20, 10), 200);
    CHECK_EQUAL(Percent<int>::CalculatePercent(20, 5), 400);
}

TEST_MEMBER_FUNCTION(Percent, PercentChange, type_type)
{
    CHECK_EQUAL(Percent<int>::PercentChange(80, 94), 17);
}

TEST_MEMBER_FUNCTION(Percent, PercentDifference, type_type)
{
    CHECK_EQUAL(Percent<int>::PercentDifference(15, 25), 50);
    CHECK_EQUAL(Percent<int>::PercentDifference(25, 15), 50);
}
