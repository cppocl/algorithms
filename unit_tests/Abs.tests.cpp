/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../abs/Abs.hpp"

TEST_FUNCTION(Abs, type)
{
    using ocl::Abs;

    CHECK_TRUE(Abs<signed char>(1) == static_cast<signed char>(1));
    CHECK_TRUE(Abs<signed short int>(1) == static_cast<signed short int>(1));
    CHECK_TRUE(Abs<signed int>(1) == static_cast<signed int>(1));
    CHECK_TRUE(Abs<signed long int>(1) == static_cast<signed long int>(1));
    CHECK_TRUE(Abs<signed long long int>(1) == static_cast<signed long long int>(1));
    CHECK_TRUE(Abs<float>(1.0) == static_cast<float>(1.0));
    CHECK_TRUE(Abs<double>(1.0) == static_cast<double>(1.0));
    CHECK_TRUE(Abs<long double>(1.0) == static_cast<long double>(1.0));

    CHECK_TRUE(Abs<signed char>(-1) == static_cast<signed char>(1));
    CHECK_TRUE(Abs<signed short int>(-1) == static_cast<signed short int>(1));
    CHECK_TRUE(Abs<signed int>(-1) == static_cast<signed int>(1));
    CHECK_TRUE(Abs<signed long int>(-1) == static_cast<signed long int>(1));
    CHECK_TRUE(Abs<signed long long int>(-1) == static_cast<signed long long int>(1));
    CHECK_TRUE(Abs<float>(-1.0) == static_cast<float>(1.0));
    CHECK_TRUE(Abs<double>(-1.0) == static_cast<double>(1.0));
    CHECK_TRUE(Abs<long double>(-1.0) == static_cast<long double>(1.0));

    CHECK_TRUE(Abs<unsigned char>(1) == static_cast<unsigned char>(1));
    CHECK_TRUE(Abs<unsigned short int>(1) == static_cast<unsigned short int>(1));
    CHECK_TRUE(Abs<unsigned int>(1) == static_cast<unsigned int>(1));
    CHECK_TRUE(Abs<unsigned long int>(1) == static_cast<unsigned long int>(1));
    CHECK_TRUE(Abs<unsigned long long int>(1) == static_cast<unsigned long long int>(1));
}
