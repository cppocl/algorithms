/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../prime/prime.hpp"
#include "../combinations/combinations.hpp"
#include "Print.hpp"
#include <utility>
#include <cstddef>
#include <set>
#include <vector>

namespace
{
    typedef unsigned long long type;

    type Multiply(std::vector<type>const & values)
    {
        type result = 1;
        for (type n : values)
            result *= n;
        return result;
    }

    void GetPrimeFactors(ocl::Integer<type, std::vector<type>> prime, std::vector<type>& primes)
    {
        typedef ocl::Combinations<type> combinations_type;
        typedef std::vector<type> container_type;
        typedef std::vector<std::vector<type>> combinations_container_type;

        primes.clear();
        std::vector<type> const& all_primes = prime.GetPrimes();

        combinations_container_type all_perms;
        combinations_type::GetAll(all_perms, all_primes);
        for (container_type const& perms : all_perms)
        {
            if (Multiply(perms) == prime.GetValue())
            {
                primes = perms;
                break;
            }
        }
    }
}

TEST(prime)
{
    typedef ocl::Integer<unsigned long long> integer_type;

    // Array of prime and non-prime numbers, with associated true or false comparison result.
    std::vector<std::pair<unsigned long long, bool>> values
    {
        { 2, true },
        { 3, true },
        { 4, false },
        { 5, true },
        { 6, false },
        { 7, true },
        { 8, false },
        { 9, false },
        { 10, false },
        { 11, true },
        { 12, false },
        { 13, true }
    };

    for (auto p : values)
    {
        CHECK_TRUE(integer_type::IsPrime(p.first) == p.second);

        integer_type i(p.first);
        CHECK_TRUE(i.IsPrime() == p.second);
    }
}
