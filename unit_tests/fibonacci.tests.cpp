/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../fibonacci/fibonacci.hpp"

TEST(fibonacci)
{
    std::size_t n = 50;

    std::map<std::size_t, std::size_t> map_cache;
    std::size_t f1 = ocl::fibonacci(n, map_cache);
    CHECK_TRUE(f1 == 12586269025ULL);

    std::vector<std::size_t> vec_cache;
    std::size_t f2 = ocl::fibonacci(n, vec_cache);
    CHECK_TRUE(f2 == 12586269025ULL);

    std::size_t f3 = ocl::fibonacci(n);
    CHECK_TRUE(f3 == 12586269025ULL);
}
