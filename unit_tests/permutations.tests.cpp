/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "../../unit_test_framework/test/Test.hpp"
#include "../permutations/permutations.hpp"
#include "Print.hpp"
#include <algorithm>
#include <time.h>
#include <vector>

TEST_MEMBER_FUNCTION(Permutations, Get, NA)
{
    typedef ocl::Permutations<std::size_t> permutations_type;
    typedef typename permutations_type::type type;
    typedef std::vector<std::size_t> container_type;
    typedef std::vector<std::vector<std::size_t>> permutations_container_type;

    {
        container_type values{ 1, 2, 3 };
        permutations_container_type perms{};

        // Test all unique permutations containing 2 values.
        permutations_type::Get(perms, values);
        std::size_t expected = permutations_type::Count(values.size());
        CHECK_TRUE(perms.size() == expected);
    }

    {
        container_type values{ 1, 2, 3, 4 };
        permutations_container_type perms{};

        // Test all unique permutations containing 2 values.
        // for (1, 2, 3, 4) permutations of length 2 = (1, 2), (1, 3), (1, 4), (2, 3), (2, 4), (3, 4)
        permutations_type::Get(perms, values);
        std::size_t expected = permutations_type::Count(values.size());
        CHECK_TRUE(perms.size() == expected);
    }
}

#if __has_include("permutations.performance.tests.cpp")
#include "permutations.performance.tests.cpp"
#endif
