/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <list>
#include <set>
#include <vector>
#include <string>

namespace ocl
{

template<typename T>
void Print(T const& value, bool eol = true)
{
    std::cout << value;
    if (eol)
        std::cout << std::endl;
}

template<typename T>
void Print(std::list<T> const& values)
{
    if (!values.empty())
    {
        bool first = true;
        for (T n : values)
        {
            if (!first)
                std::cout << ",";
            std::cout << n;
            first = false;
        }
        std::cout << std::endl;
    }
}

template<typename T>
void Print(std::set<T> const& values)
{
    if (!values.empty())
    {
        bool first = true;
        for (T n : values)
        {
            if (!first)
                std::cout << ",";
            std::cout << n;
            first = false;
        }
        std::cout << std::endl;
    }
}

template<typename T>
void Print(std::vector<T> const& values)
{
    if (!values.empty())
    {
        bool first = true;
        for (T n : values)
        {
            if (!first)
                std::cout << ",";
            std::cout << n;
            first = false;
        }
        std::cout << std::endl;
    }
}

template<typename T>
void Print(std::string const& msg, std::list<T> const& values)
{
    std::cout << msg;
    Print(values);
}

template<typename T>
void Print(std::string const& msg, std::set<T> const& values)
{
    std::cout << msg;
    Print(values);
}

template<typename T>
void Print(std::string const& msg, std::vector<T> const& values)
{
    std::cout << msg;
    Print(values);
}

} // namespace ocl
