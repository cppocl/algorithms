/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ALGORITHMS_PERMUTATIONS_COMBINATIONS_HPP
#define OCL_GUARD_ALGORITHMS_PERMUTATIONS_COMBINATIONS_HPP

#include <cstddef>
#include <set>
#include <list>
#include <vector>

namespace ocl
{

// Get all permutations from a set of numbers, where order is not relevant.
// E.g. [1,2] and [2,1] are treated as the same.
//
// All permutations of [1,2,3] would be [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]
template<typename Type>
class Permutations
{
public:
    typedef Type type;

    // Get all the combinations for the input array including an empty element.
    static void Get(std::vector<std::vector<type>>& output, std::vector<type> const& input)
    {
        output.clear();
        for (type value : input)
        {
            std::vector<type> existing(1U, value);
            Get(output, input, existing);
        }
    }

    // Calculate the number of permutations for a set of numbers.
    // E.g. numbers 1, 2, 3 the result will be 6 permutations.
    // [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]
    static std::size_t Count(std::size_t elements)
    {
        return Factorial<std::size_t>(elements);
    }

    // Calculate the number of permutations for a set of numbers.
    // E.g. numbers 1, 2, 3 the result will be 6 permutations.
    // [1,2,3], [1,3,2], [2,1,3], [2,3,1], [3,1,2], [3,2,1]
    static std::size_t Count(std::size_t elements, std::size_t amount)
    {
        return Factorial<std::size_t>(elements) / Factorial<std::size_t>(elements - amount);
    }

private:
    template<typename Type>
    static Type Factorial(Type n) noexcept
    {
        Type total = 1;
        while (n > 1)
            total *= n--;
        return total;
    }

    static void Get(std::vector<std::vector<type>>& output,
                    std::vector<type> const& input,
                    std::vector<type> const& existing)
    {
        std::vector<type> remainder;
        Separate(input, existing, remainder);
        if (remainder.size() > 2U)
        {
            for (type item : remainder)
            {
                std::vector<type> existing2(existing);
                existing2.push_back(item);
                Get(output, input, existing2);
            }
        }
        else
        {
            Append(output, existing, remainder[0], remainder[1]);
            Append(output, existing, remainder[1], remainder[0]);
        }
    }

    static void Separate(std::vector<type> const& total,
                         std::vector<type> const& existing,
                         std::vector<type>& remainder)
    {
        if (!existing.empty())
        {
            for (type item : total)
                if (std::find(existing.begin(), existing.end(), item) == existing.end())
                    remainder.push_back(item);
        }
        else
            remainder = total;
    }

    static void Append(std::vector<std::vector<type>>& output,
                       std::vector<type> const& values,
                       type value1,
                       type value2)
    {
        std::vector<type>& row = output.emplace_back();
        std::size_t size = values.size();
        row.resize(size + 2);
        std::copy(values.begin(), values.end(), row.begin());
        row[size] = value1;
        row[size + 1] = value2;
    }
};

} // namespace ocl

#endif // OCL_GUARD_ALGORITHMS_PERMUTATIONS_COMBINATIONS_HPP
