/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cstddef>
#include <ctime>
#include <iostream>
#include <map>
#include <vector>

#ifndef OCL_GUARD_FIBONACCI_HPP
#define OCL_GUARD_FIBONACCI_HPP

/*
  The Fibonacci sequence is the sum of the two preceding values, usually starting with 1 and 1,
  although the solution is sometimes solved by starting with 0 and 1.

  This is an example of a Fibonacci sequence:

  1, 1, 2, 3, 5, 8, 13, 21
*/

namespace ocl
{

// Recursively find the nth value in the Fibonacci sequence and
// cache the already calculated values in a map.
std::size_t fibonacci(std::size_t n, std::map<std::size_t, std::size_t>& cache)
{
    auto iter = cache.find(n);
    if (iter != cache.end())
        return iter->second;
    std::size_t f = n <= 2 ? 1 : fibonacci(n - 1, cache) + fibonacci(n - 2, cache);
    cache.insert(std::pair<std::size_t, std::size_t>(n, f));
    return f;
}

// Iteratively find the nth value in the Fibonacci sequence and
// cache the already calculated values in a vector.
std::size_t fibonacci(std::size_t n, std::vector<std::size_t>& cache)
{
    if (n <= 2)
        return 1;
    if (cache.size() <= n)
        cache.resize(n + 1, 0U);
    else if (cache[n] != 0)
        return cache[n];

    std::size_t n1 = 1;
    std::size_t n2 = 1;
    for (std::size_t c = 3; c <= n; ++c)
    {
        std::size_t next = n1 + n2;
        n1 = n2;
        n2 = next;
    }

    cache[n] = n2;
    return n2;
}

// Iteratively find the nth value in the Fibonacci sequence.
std::size_t fibonacci(std::size_t n)
{
    if (n <= 2)
        return 1;
    std::size_t n1 = 1;
    std::size_t n2 = 1;
    for (std::size_t c = 3; c <= n; ++c)
    {
        std::size_t next = n1 + n2;
        n1 = n2;
        n2 = next;
    }

    return n2;
}

} // namespace ocl

#endif // OCL_GUARD_FIBONACCI_HPP
