/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ALGORITHMS_PERCENT_HPP
#define OCL_GUARD_ALGORITHMS_PERCENT_HPP

#include "../abs/Abs.hpp"

namespace ocl
{

template<typename Type>
class Percent
{
/// Types and constants.
public:
    typedef Type type;

    static bool const is_signed_type = static_cast<Type>(-1) < static_cast<Type>(0);

/// Construction.
public:
    Percent()
        : m_percent(0)
    {
    }

    Percent(Type numerator,   // top part of fraction
            Type denominator) // bottom part of fraction)
        : m_percent((numerator * static_cast<Type>(100)) / denominator)
    {
    }

    void SetPercent(Type numerator,   // top part of fraction
                    Type denominator) // bottom part of fraction)
    {
        m_percent = CalculatePercent(numerator, denominator);
    }

    void SetPercentChange(Type start, Type end)
    {
        m_percent = PercentChange(start, end);
    }

    void SetPercentDifference(Type value1, Type value2)
    {
        m_percent = PercentDifference(value1, value2);
    }

    Type GetPercent() const
    {
        return m_percent;
    }

    operator Type() const
    {
        return m_percent;
    }

/// Alternative helper functions.
public:
    /// Calculate percentage of a value, i.e. 40 (numerator) out of 160 (denominator) = 25%.
    static Type CalculatePercent(Type numerator,   // top part of fraction
                                 Type denominator) // bottom part of fraction))
    {
        return (numerator * static_cast<Type>(100)) / denominator;
    }

    /// Calculate the percentage of change between a start and end value.
    /// Note: start can be considered to be the old value and end to be considered the new value.
    /// Note: If start value is greater than end value,
    ///       the percentage of change has a negative meaning for unsigned types.
    static Type PercentChange(Type start, Type end)
    {
        // Formula for change is: (start - end) / start
        Type diff = (is_signed_type || (end >= start)) ? end - start : start - end;
        return (diff * static_cast<Type>(100)) / Abs(start);
    }

    /// Calculate the percentage of difference between two values.
    /// Note: If you want to know the percentage of difference between the length of two cars,
    ///       then this formula will give you a difference.
    static Type PercentDifference(Type value1, Type value2)
    {
        // Formula for the difference of two values as an average is:
        //    ( (value2 - value1) / ((value2 + value1) / 2) ) * 100
        Type diff = (is_signed_type || (value2 >= value1)) ? value1 - value2 : value2 - value1;
        return (Abs(diff) * static_cast<Type>(100)) / (Abs(value1 + value2) / 2);
    }

private:
    Type m_percent;
};

} // namespace ocl

#endif // OCL_GUARD_ALGORITHMS_PERCENT_HPP
