/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <cassert>
#include <ctime>
#include <iostream>

namespace ocl
{

void StringFlip(char* start, char* end)
{
    char* mid = start + ((end - start) / 2);
    for (; start <= mid; ++start, --end)
    {
        char temp = *start;
        *start = *end;
        *end = temp;
    }
}

void WordFlip(char* myString)
{
    char* end = myString;

    while (*end != '\0')
        ++end;
    --end;

    StringFlip(myString, end);
    char* word_start = myString;

    for (;myString < end; ++myString)
    {
        if (*myString == ' ')
        {
            StringFlip(word_start, myString - 1);
            word_start = myString + 1;
        }
    }

    if (word_start < end)
        StringFlip(word_start, end);
}

} // namespace ocl
