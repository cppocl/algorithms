/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include <vector>

// Generate a sequence of 
template<typename T>
void CollatzSequence(T value, std::vector<T>& sequence)
{
    sequence.push_back(value);
    if (value > 1)
    {
        if ((value % 2) != 0)
            CollatzSequence((value * 3) + 1, sequence);
        else
            CollatzSequence(value / 2, sequence);
    }
}

template<typename T = long long>
std::size_t LongestCollatzSequence(T start)
{
    std::vector<T> sequence;
    CollatzSequence(start, sequence);
    return sequence.size();
}
