/*
Copyright 2017 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ALGORITHMS_ABS_HPP
#define OCL_GUARD_ALGORITHMS_ABS_HPP

namespace ocl
{

template<typename NumericType>
inline NumericType Abs(NumericType value)
{
    static bool const is_signed_type = static_cast<NumericType>(-1) <
                                       static_cast<NumericType>(0);

    bool const is_negative = is_signed_type && (value < static_cast<NumericType>(0));

    return is_negative ? value * static_cast<NumericType>(-1) : value;
}

} // namespace ocl

#endif // OCL_GUARD_ALGORITHMS_ABS_HPP
