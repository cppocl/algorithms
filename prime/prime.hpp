/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ALGORITHMS_PRIME_PRIME_HPP
#define OCL_GUARD_ALGORITHMS_PRIME_PRIME_HPP

#include <algorithm>
#include <list>
#include <set>
#include <vector>

namespace ocl
{

// Integer class for working with prime numbers and caching the prime numbers in a cache.
template<typename IntType = unsigned long long, typename ContainerType = std::set<IntType>>
class Integer
{
public:
    typedef IntType int_type;
    typedef ContainerType container_type;

    Integer() noexcept
        : m_value(0)
        , m_largest_cached_prime(0)
    {
    }

    Integer(int_type value) noexcept
        : m_value(value)
        , m_largest_cached_prime(0)
    {
    }

    Integer(Integer const& value)
        : m_value(value.m_value)
        , m_largest_cached_prime(value.m_largest_cached_prime)
        , m_cached_primes(value.m_cached_primes)
    {
    }

    Integer(Integer&& value) noexcept
        : m_value(value.m_value)
        , m_largest_cached_prime(value.m_largest_cached_prime)
        , m_cached_primes(std::move(value.m_cached_primes))
    {
    }

    Integer& operator=(Integer const& value)
    {
        m_value = value.m_value;
        m_largest_cached_prime = value.m_largest_cached_prime;
        m_cached_primes = value.m_cached_primes;
        return *this;
    }

    Integer& operator=(Integer&& value) noexcept
    {
        m_value = value.m_value;
        m_largest_cached_prime = value.m_largest_cached_prime;
        m_cached_primes = std::move(value.m_cached_primes);
        return *this;
    }

    Integer& operator=(int_type value) noexcept
    {
        m_value = value;
        return *this;
    }

    int_type GetValue() const noexcept
    {
        return m_value;
    }

    // Return true when the value is a prime number.
    // NOTE: The function is not constant as it might update the cache.
    bool IsPrime()
    {
        if (m_value > 2)
        {
            CachePrimes(m_value);
            return Exists(m_value, m_cached_primes);
        }
        return m_value > 0;
    }

    // Clear the value and cache of prime numbers.
    void Clear()
    {
        m_value = 0;
        m_largest_cached_prime = 0;
        m_cached_primes.clear();
    }

    // Get all the prime numbers up to the existing value.
    container_type const& GetPrimes()
    {
        CachePrimes(m_value);
        return m_cached_primes;
    }

    // Check if a value is a prime number without the use of the internally cached prime numbers.
    static bool IsPrime(int_type value, int_type start = 3)
    {
        if ((value % 2) == 0)
            return value == 2; // Even numbers are never prime, except for 2.
        if ((start % 2) == 0)
            ++start; // Never start on an even number.
        for (int_type n = start; n < value; n += 2)
            if ((value % n) == 0)
                return false;
        return true;
    }

private:
    // Cache all prime numbers up to the specified limit.
    void CachePrimes(int_type limit, int_type start = 3)
    {
        if (m_largest_cached_prime > start)
            start = m_largest_cached_prime;

        if ((start % 2) == 0) // Never start on an even number.
            ++start;

        for (int_type n = start; n <= limit; n += 2)
        {
            if (!Exists(n, m_cached_primes) && !Divisible(n, m_cached_primes) && IsPrime(n, start))
            {
                Add(n, m_cached_primes);
                start = n;
            }
        }

        m_largest_cached_prime = start;
    }

    template<typename T>
    static bool Divisible(T const& value, std::list<T>& values)
    {
        for (auto n : values)
            if ((value % n) == 0)
                return true;
        return false;
    }

    template<typename T>
    static bool Divisible(T const& value, std::set<T>& values)
    {
        for (auto n : values)
            if ((value % n) == 0)
                return true;
        return false;
    }

    template<typename T>
    static bool Divisible(T const& value, std::vector<T>& values)
    {
        for (auto n : values)
            if ((value % n) == 0)
                return true;
        return false;
    }

    template<typename T>
    static bool Exists(T const& value, std::list<T>& values)
    {
        return values.find(value) != values.end();
    }

    template<typename T>
    static bool Exists(T const& value, std::set<T>& values)
    {
        return values.find(value) != values.end();
    }

    template<typename T>
    static bool Exists(T const& value, std::vector<T>& values)
    {
        return std::find(values.begin(), values.end(), value) != values.end();
    }

    template<typename T>
    static void Add(T const& value, std::list<T>& values) { values.push_back(value); }

    template<typename T>
    static void Add(T const& value, std::set<T>& values) { values.insert(value); }

    template<typename T>
    static void Add(T const& value, std::vector<T>& values) { values.push_back(value); }

    template<typename T>
    static void Copy(std::vector<T>& dst, std::list<T> const& src)
    {
        dst.reserve(src.size());
        for (auto v : src)
            dst.push_back(v);
    }

    template<typename T>
    static void Copy(std::vector<T>& dst, std::set<T> const& src)
    {
        dst.reserve(src.size());
        for (auto v : src)
            dst.push_back(v);
    }

    template<typename T>
    static void Copy(std::vector<T>& dst, std::vector<T> const& src)
    {
        dst = src;
    }

private:
    int_type m_value;
    int_type m_largest_cached_prime;
    container_type m_cached_primes;
};

} // namespace ocl

#endif // OCL_GUARD_ALGORITHMS_PRIME_PRIME_HPP
