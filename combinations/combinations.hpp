/*
Copyright 2024 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ALGORITHMS_PERMUTATIONS_COMBINATIONS_HPP
#define OCL_GUARD_ALGORITHMS_PERMUTATIONS_COMBINATIONS_HPP

#include <cstddef>
#include <set>
#include <list>
#include <vector>

namespace ocl
{

// Get all combinations from a set of numbers, where order is not relevant.
// E.g. [1,2] and [2,1] are treated as the same.
//
// All combinations of [1,2,3] for two numbers would be [1,2], [1,3] and [2,3].
template<typename Type>
class Combinations
{
public:
    typedef Type type;

    // Get all the combinations for the input array including an empty element.
    static void GetAll(std::vector<std::vector<type>>& output, std::vector<type> const& input)
    {
        if (!input.empty())
        {
            output.clear();
            output.reserve(Count(input.size()));

            for (type element : input)
            {
                (void)output.emplace_back(1U, element);
                std::size_t size = output.size() - 1U;

                for (std::size_t pos = 0; pos < size; ++pos)
                {
                    std::size_t curr_size = output[pos].size();
                    std::vector<type>& new_sub_set = output.emplace_back(curr_size + 1U);
                    std::vector<type>& sub_set = output[pos];
                    std::copy(sub_set.begin(), sub_set.end(), new_sub_set.begin());
                    new_sub_set[curr_size] = element;
                }
            }
        }
    }

    // Get the combinations that match the length of count for the input array.
    static void Get(std::vector<std::vector<type>>& output, std::vector<type> const& input, std::size_t count)
    {
        if (!input.empty())
        {
            output.clear();
            output.reserve(Count(input.size(), count));
            std::vector<std::vector<type>> all_output;
            all_output.reserve(Count(input.size()));

            for (type element : input)
            {
                std::vector<type>& first_sub_set = all_output.emplace_back(1U, element);
                if (first_sub_set.size() == count)
                    output.push_back(first_sub_set);
                std::size_t size = all_output.size() - 1U;

                for (std::size_t pos = 0; pos < size; ++pos)
                {
                    std::size_t curr_size = all_output[pos].size();
                    std::vector<type>& new_sub_set = all_output.emplace_back(curr_size + 1U);
                    std::vector<type>& sub_set = all_output[pos];
                    std::copy(sub_set.begin(), sub_set.end(), new_sub_set.begin());
                    new_sub_set[curr_size] = element;
                    if (new_sub_set.size() == count)
                        output.push_back(new_sub_set);
                }
            }
        }
    }

    // Get the total number of unique combinations for a set of numbers with n items.
    // E.g. numbers 1, 2, 3 matching all numbers will result in 7 total combinations.
    // [1], [1,2], [1,3], [1,2,3], [2], [2,3], [3]
    static std::size_t Count(std::size_t elements)
    {
        auto GetPow = [](auto base, auto exponent)
        {
            auto p = base;
            while (exponent-- > 1)
                p *= base;
            return p;
        };
        constexpr std::size_t base = 2U;
        return GetPow(base, elements) - 1U;
    }

    // Calculate the number of combinations for a set of numbers with a given amount of combinations required.
    // E.g. numbers 1, 2, 3 matching 2 numbers will result in 3 total combinations.
    // [1,2], [1,3], [2,3]
    static std::size_t Count(std::size_t elements, std::size_t amount)
    {
        auto Factorial = [](std::size_t n)
        {
            std::size_t total = 1;
            while (n > 1)
                total *= n--;
            return total;
        };

        std::size_t count = Factorial(elements) / (Factorial(amount) * Factorial(elements - amount));
        return count;
    }
};

} // namespace ocl

#endif // OCL_GUARD_ALGORITHMS_PERMUTATIONS_COMBINATIONS_HPP
