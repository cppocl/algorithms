# Find Combinations

Implementation of the Python combinations algorithms.

```
# Python example of getting all the combinations, including an empty item.
def combinations(inp):
    outp = [[]]
    for element in inp:
        for sub_set in outp.copy():
            new_sub_set = sub_set + [element]
            outp.append(new_sub_set)
    return outp

# Python example of getting all the combinations, without an empty item.
def combinations2(inp):
    outp = []
    for element in inp:
        if not outp:
            outp.append([element])
        else:
            outp_copy = outp.copy()
            outp.append([element])
            for sub_set in outp_copy:
                new_sub_set = sub_set + [element]
                outp.append(new_sub_set)
    return outp

# Time the combinations of 20 elements.
timeit.timeit("len(combinations([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20]))",
                setup="from __main__ import combinations", number=1)
```
