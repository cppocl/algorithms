/*
Copyright 2016 Colin Girling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef OCL_GUARD_ALGORITHMS_ARRAYSEARCHALGORITHMS_HPP
#define OCL_GUARD_ALGORITHMS_ARRAYSEARCHALGORITHMS_HPP

#include "../../typeinfo/TypeInfo.hpp"
#include "../../typeinfo/TypeSigned.hpp"
#include "../../typeinfo/TypeCompare.hpp"

namespace ocl
{

// Default algorithm compares primitive types, but not string or char* types.
// Use StringCompare for CompareType template argument when wanting to search
// arrays containing strings.
// SizeType is intentionally signed as the algorithm performs better,
// as the offset will be optimized out at compile time.
template<typename Type, typename SizeType = signed long, typename CompareType = TypeCompare<Type> >
class ArraySearchAlgorithm
{
public:
    typedef typename TypeInfo<Type>::in_type in_type;

    // Binary search overloads...
    // values must be ordered or binary search functions will have unexpected behavior.
    //

    // If not found then return the nearest position for inserting the value.
    static bool BinarySearch(Type const* values,
                             SizeType size,
                             in_type value_to_find,
                             SizeType& nearest_pos)
    {
        bool found;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            SizeType start = 0;
            SizeType end = size - static_cast<SizeType>(1);
            SizeType mid;
            int cmp;

            do
            {
                mid = (start + end) >> static_cast<SizeType>(1);
                Type to_compare = values[mid];
                cmp = (value_to_find < to_compare) ? -1 : (to_compare < value_to_find ? 1 : 0);
                if (cmp < 0)
                {
                    if (mid > 0)
                        end = mid - static_cast<SizeType>(1);
                    else
                        break;
                }
                else if (cmp > 0)
                    start = mid + static_cast<SizeType>(1);
                else
                    break;
            }
            while (start <= end);

            mid = (start + end) >> static_cast<SizeType>(1);
            SizeType const offset = cmp > 0 ? 1 : 0;
            nearest_pos = mid + offset;
            found = cmp == 0;
        }
        else
        {
            nearest_pos = static_cast<SizeType>(0);
            found = false;
        }

        return found;
    }

    static bool BinarySearch(Type const* values,
                             SizeType size,
                             in_type value_to_find)
    {
        bool found;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            SizeType start = 0;
            SizeType end = size - static_cast<SizeType>(1);
            SizeType mid;
            int cmp;

            do
            {
                mid = (start + end) >> static_cast<SizeType>(1);
                Type to_compare = values[mid];
                cmp = (value_to_find < to_compare) ? -1 : (to_compare < value_to_find ? 1 : 0);
                if (cmp < 0)
                {
                    if (mid > 0)
                        end = mid - static_cast<SizeType>(1);
                    else
                        break;
                }
                else if (cmp > 0)
                    start = mid + static_cast<SizeType>(1);
                else
                    break;
            }
            while (start <= end);

            found = cmp == 0;
        }
        else
            found = false;

        return found;
    }


    // Sequential search overloads...
    //

    // Iterate from start to end for the value to find.
    // If not found then return the nearest position for inserting the value.
    // If the value is greater than any value, nearest_pos will be size.
    static bool SequentialSearch(Type const* values,
                                 SizeType size,
                                 in_type value_to_find,
                                 SizeType& nearest_pos)
    {
        bool found = false;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            SizeType best_pos = size;
            Type const* end_value = values + size;

            for (Type const* curr_value = values; curr_value < end_value; ++curr_value)
            {
                in_type compare_value = *curr_value;
                SizeType curr_pos = static_cast<SizeType>(curr_value - values);
                if (value_to_find < compare_value)
                {
                    // if best value so far is less than value_to_find
                    // then this compare is nearer than the previous best value.
                    if ((values[best_pos] < value_to_find) || (best_pos == size))
                        best_pos = curr_pos;
                }
                else if (!(compare_value < value_to_find))
                {
                    best_pos = curr_pos;
                    found = true;
                    break;
                }
            }
            // If best_pos was not set then the value to find
            // is larger than all values in the array, so set last position.
            nearest_pos = best_pos;
        }
        else
            nearest_pos = static_cast<SizeType>(0);

        return found;
    }

    // Iterate from start to end for the value to find.
    static bool SequentialSearch(Type const* values,
                                 SizeType size,
                                 in_type value_to_find)
    {
        bool found = false;

        if ((size > static_cast<SizeType>(0)) && (values != nullptr))
        {
            Type const* end_value = values + size;

            for (Type const* curr_value = values; curr_value < end_value; ++curr_value)
                if (value_to_find == *curr_value)
                {
                    found = true;
                    break;
                }
        }

        return found;
    }

    // Search using binary search for ordered arrays or sequential search for unordered arrays.
    //

    // Search using the best algorithm based on if the array supplied is ordered.
    template<bool const ordered>
    static bool Search(Type const* values,
                       SizeType size,
                       in_type value_to_find,
                       SizeType& nearest_pos)
    {
        return ordered ? BinarySearch(values, size, value_to_find, nearest_pos)
                       : SequentialSearch(values, size, value_to_find, nearest_pos);
    }

    template<bool const ordered>
    static bool Search(Type const* values,
                       SizeType size,
                       in_type value_to_find)
    {
        return ordered ? BinarySearch(values, size, value_to_find)
                       : SequentialSearch(values, size, value_to_find);
    }
};

} // namespace ocl

#endif // OCL_GUARD_ALGORITHMS_ARRAYSEARCHALGORITHMS_HPP
